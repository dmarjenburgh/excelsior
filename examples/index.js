const ex = require("../lib/dom");

const header = ex.h1({ className: "header" }, "Hello");

const main = ex.div({},
    header,
    ex.section({},
        ex.p({}, "This is the main section")));

ex.mount(main, document.getElementById("app"));
