const g = require("genshi");
function mount(element, target) {
    target.appendChild(element);
}

function elementConstructor(tagName) {
    return function (attrs, ...children) {
        const el = document.createElement(tagName);
        Object.keys(attrs).forEach(key => {
            const aValue = attrs[key];
            if (g.isCell)
            el[key] = attrs[key];
        });
        children.forEach(ch => {
            ch = typeof ch === "string" ? document.createTextNode(ch) : ch;
            el.appendChild(ch);
        });
        return el;
    };
}

exports.h1 = elementConstructor("h1");
exports.div = elementConstructor("div");
exports.section = elementConstructor("section");
exports.p = elementConstructor("p");

exports.mount = mount;